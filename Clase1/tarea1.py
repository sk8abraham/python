#!/usr/bin/python
# -*- coding: utf 8 -*-
#Isaias Abraham Manzano Cruz

primos=[]
def es_palindromo(cadena):
    """
    Recibe una cadena y verifica si dentro, hay un palindromo y devuelve el más grande
    """
    posibles=[]
    for i in range (len(cadena)):
        for j in range (len(cadena)):
            if cadena[i]==cadena[j]:
                aux=cadena[i:j+1]
                if aux==aux[::-1] and len(aux)>1:
                    posibles.append(aux)
    return max(posibles,key=len)


def es_primo(numero):
    """
    Recibe un numero y verifica que sea primo, devolviendo: verdadero o falso
    """
    if numero<2:
        return False
    elif numero>2 and numero%2==0:
        return False
    else:
        for i in range(3,int(numero**0.5)+1):
            if numero%i==0:
                return False
            else:
                return True
                
def n_primos(primos,inicio):
    """
    Funcion que calcula los primeros n numeros primos, recibe un entero indicando el numero de numeros primos a calcular y un entero indicando a apartir de que numero empieza
    """
    contador=0
    if contador==primos:
        return primos
    else:
        if es_primo(inicio):
            primos.append(inicio)
            contador+=1
        else:
            n_primos(primos,inicio+1)
    return primos
   
    



#Pruebas
print "Palindromo mas grande: %s"% es_palindromo("cadenadondanitalavalatinaespalindromo")
if es_primo(17):
    print 'Es primo'
else:
    print 'No es primo'
lista_primos=n_primos(10,1)
print lista_primos