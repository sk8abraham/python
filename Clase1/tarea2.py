#!/usr/bin/python
# -*- codiing = utf-8 -*-
#Manzano Cruz Isaias Abraham
import random
import numpy as np
#random.sample(cadena,1)
chars='#$%&.,-@'
numeros='123456789'
letras='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
def genera_passwd(longitud,passwd):
    """
   Funcion que genera una contrasena segura de minimo 15 caracteres, recibe la longitud de la contraseña y una cadena vacia
    """
    if longitud==1:
        return str(random.sample(letras,1))
    elif longitud==15:
        passwd+=str(random.sample(chars,1))+str(genera_passwd(longitud-1,passwd))
        passwd=passwd.replace(']','').replace('[','').replace("'",'')
    elif longitud==14:
        passwd+=str(random.sample(numeros,1))+str(genera_passwd(longitud-1,passwd))
    else:
        passwd+=str(random.sample(letras,1))+str(genera_passwd(longitud-1,passwd))

    return passwd

cadena=genera_passwd(15,'')
print 'Contrasena: %s'% cadena
