#!/usr/bin/python
# -*- coding: utf-8 -*-
#UNAM-CERT

aprobados = []

def aprueba_becario(nombre_completo):
    nombre_separado = nombre_completo.split()
    for n in nombre_separado:
        n=str(n[0].upper())+str(n[1:].lower())
        if n in ['Manuel', 'Valeria', 'Alejandro', 'Luis', 'Enrique','Omar','Abraham','Oscar']:
            return False
    aprobados.append(nombre_completo.upper())
    aprobados.sort()
    return True

def elimina_becario(nombre_completo):
    try:
        aprobados.remove(nombre_completo.upper())
        return True
    except Exception:
        return False

becarios = ['Cervantes Varela JUAN MaNuEl',
            'Leal González IgnaciO',
            'Ortiz Velarde valeria',
            'Martínez Salazar LUIS ANTONIO',
            'Rodríguez Gallardo pedro alejandro',
            'Tadeo Guillén DiAnA GuAdAlUpE',
            'Ferrusca Ortiz jorge luis',
            'Juárez Méndez JeSiKa',
            'Pacheco Franco jesus ENRIQUE',
            'Vallejo Fernández RAFAEL alejanDrO',
            'López Fernández serVANDO MIGuel',
            'Hernández González ricaRDO OMAr',
            'Acevedo Gómez LAura patrICIA',
            'Manzano Cruz isaías AbrahaM',
            'Espinosa Curiel OscaR']
for b in becarios:
    if aprueba_becario(b):
        print 'APROBADOO: ' + b
    else:
        print 'REPROBADO: ' + b


#Prueba de eliminación no satisfactoria
bec1="Manzano Cruz Isaías Abraham"
bec2="LóPEZ Fernández serVANDO MIGuel"

print "Tratando de eliminar a %s"%bec1
if elimina_becario(bec1):
    print "Eliminado"
else:
    print"No eliminado"

#Prueba de eliminación satisfactoria
print "Intentando eliminar a %s"%bec2
if elimina_becario(bec2):
    print "Eliminado"
else:
    print"No eliminado"
