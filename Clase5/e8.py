<#!/usr/bin/python
# -*- coding: utf-8 -*-
#Manzano Cruz Isaias Abraham
import re

def valida_ipv4(fl_ipv4):
    '''
    Funcion que evalua las posibles direcciones ip de un archivo
    Recibe un archivo y evalua las direcciones ip contra una expresion regular
    '''
    ipv4_re=r"(([2]?[0-5]{0,2])1]?[0-9]{0,2}\.[2]?[0-5]{0,2}[1]?[0-9]{0,2}\.[2]?[0-5]{0,2}[1]?[0-9]{0,2}\.[2]?[0-5]{0,2}[1]?[1-9]{0,2})"
    with open(fl_ipv4,'r') as f_ipv4:
        for ip in f_ipv4.readlines():
            ipv4=ip.strip('\n')
            if re.search(ipv4,ipv4_re):
                print 'Ip valida: %s'%ipv4
            else:
                print 'Ip no valida %s'%ipv4

def valida_correo(fl_correo):
    '''
    Funcion que evalua posibles direcciones de correo electronico
    Recibe un archivo y evalua los correos electronicos contra una expresion regular
    '''
    correo_re=r"(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9]+)\.([a-zA-Z0-9].*))"
    with open(fl_correo,'r') as f_correo:
        for corre in f_correo.readlines():
            correo=corre.strip('\n')
            if re.search(correo,correo_re):
                print 'Correo valido: %s' %correo
            else:
                print 'Correo no valido: %s'%correo

valida_ipv4('ips')
valida_correo('correos')
