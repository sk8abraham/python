#!/usr/bin/python
# -*- coding: utf-8 -*-
#UNAM-CERT
#Manzano Cruz Isaias Abraham

from datetime import datetime as date
import xml.etree.ElementTree as ET
from sys import argv
from hashlib import sha256
from hashlib import md5

hosts=[]

class Hosts:
    '''
    Definicion de la clase con los atributos de un host
    '''
    def __init__(self,ip,status,hostname,ssh,dns,http,https,product_http,product_https):
        self.ip = ip
        self.status = status
        self.hostname = hostname
        self.ssh = ssh
        self.dns = dns
        self.http = http
        self.https = https
        self.product_http = product_http
        self.product_https = product_https

    #def __str__(self):
        #return '%s,%s,%s,%s,%s,%s,%s,%s,%s\n' % (self.ip, self.status, self.hostname, self.ssh, self.dns, self.http, self.https, self.product_http, self.product_https)

    def __str__(self):
        '''
        Definición del método str para que el objeto pueda ser convertido a cadena
        '''
        return '%s, %s, %s, %s, %s\n' % (self.ip, self.status, self.hostname, self.ssh, self.product_http)

def lee_xml(archivo_nmap):
    '''
    Funcion que recibe un archivo xml extraido de un escaneo con nmap, recibe un archivo de nmap
    Genera una lista de objetos con los atributos de cada host en el archivo xml
    '''
    ip=''
    status=''
    hostname=''
    ssh=''
    dns=''
    http=''
    https=''
    product_http=''
    product_https=''
    with open(archivo_nmap,'r') as nmap:
        raiz=ET.fromstring(nmap.read())
        for host in raiz.findall('host'):
            status='status: '+host.find('status').get('state')
            ip = 'ip: '+host.find('address').get('addr')
            hostnm = host.findall('hostnames')
            for hstame in hostnm:
                try:
                    hostname = 'hostname: '+hstame.find('hostname').get('name')
                except:
                    hostname = 'No hostname'
            puertos = host.findall('ports')
            for puerto in puertos:
                for portid in puerto.findall('port'):
                    if portid.get('portid')=='22':
                        if portid.find('state').get('state') == 'open':
                            ssh = 'ssh open'
                        else:
                            ssh= 'ssh closed'
                    elif portid.get('portid')=='53':
                        if portid.find('state').get('state') == 'open':
                            dns = 'dns open'
                        else:
                            dns= 'dns closed'
                    elif portid.get('portid')=='80':
                        if portid.find('state').get('state') == 'open':
                            http = 'http open'
                            product_http = portid.find('service').get('product')
                        else:
                            http = 'http closed'
                    elif portid.get('portid')=='443':
                        if portid.find('state').get('state') == 'open':
                            https = 'https open'
                            product_https = portid.find('service').get('product')
                        else:
                            https = 'https closed'
            host_01 = Hosts(ip,status,hostname,ssh,dns,http,https,product_http,product_https)
            hosts.append(host_01)
            ip=''
            status=''
            hostname='No hostname'
            ssh='ssh closed'
            dns='dns closed'
            http='http closed'
            https='https closed'
            product_http='Other product'
            product_https='Other product'


def imprime_resultados(*lista):
    '''
    Funcion que recibe una lista con los hallazgos del escaneo
    Genera un archivo con información de la cantidad de hosts prendidos, apagados
    con los puertos 22,53,80 y 443 abiertos, con nombre de dominio y los servidores
    http usados
    '''
    with open('resultados.txt','a') as f_resultados:
        for linea in lista:
            print linea
            f_resultados.write(linea+'\n')

def obtiene_cantidad():
    '''
    Función que genera cuenta los numeros de hosts prendidos, apagados
    con los puertos 22,53,80 y 443 abiertos, con nombre de dominio y los servidores
    http usados
    '''
    hosts_up = 0
    hosts_down = 0
    hosts_ssh = 0
    hosts_dns = 0
    hosts_http = 0
    hosts_https = 0
    hosts_hostname = 0
    hosts_apache = 0
    hosts_nginx = 0
    hosts_honey = 0
    hosts_otros = 0
    #hosts_prendidos=(lambda x: x.status.count('status: up'))(hosts)
    #print 'Lambda, hosts prendidos: %d'%hosts_prendidos
    for host in hosts:
        #if host.status == 'status: up':
        if 'up' in host.status:
            hosts_up += 1
            if 'open' in host.ssh:
                hosts_ssh += 1
            if 'open' in host.dns:
                hosts_dns += 1
            if 'open' in host.http:
                hosts_http +=1
            if 'open' in host.https:
                hosts_https+=1
            if host.hostname != 'No hostname':
                hosts_hostname +=1
            try:
                if 'Apache' in host.product_http or 'Apache' in host.product_https:
                    hosts_apache += 1
                elif 'nginx' in host.product_http or 'nginx' in host.product_https:
                    hosts_nginx += 1
                elif 'Dionaea' in host.product_http or 'Dionaea' in host.product_https:
                    hosts_honey += 1
                else:
                    hosts_otros += 1
                    
            except Exception:
                pass

        else:
            hosts_down+=1

    prendidos = 'Hosts prendidos: %d'% hosts_up
    apagados = 'Hosts apagados: %d'% hosts_down
    puerto22 = 'Hosts con el puerto 22 abierto: %d'% hosts_ssh
    puerto53 = 'Hosts con el puerto 53 abierto: %d'% hosts_dns
    puerto80 = 'Hosts con el puerto 80 abierto: %d'% hosts_http
    puerto443= 'Hosts con el puerto 443 abierto: %d'% hosts_https
    dominio = 'Hosts con nombre de dominio: %d'% hosts_hostname
    apache = 'Hosts con Apache: %d'% hosts_apache
    nginx = 'Hosts con Nginx: %d'% hosts_nginx
    honey = 'Hosts honeypots: %d'% hosts_honey
    otros = 'Hosts usando otros servicios: %d'% hosts_otros
    imprime_resultados(prendidos,apagados,puerto22,puerto53,puerto80,puerto443,dominio,apache,nginx,honey,otros)


def imprime_csv():
    '''
    Funcion que imprime en un archivo la informacion de los hosts en dormato csv
    '''
    with open('report.csv','w') as f_reporte:
        for host in hosts:
            f_reporte.write(str(host))

def sha2_md5(archivo):
    '''
    Funcion que obtiene el md5 y sha256 de un archivo y los imprime en pantalla
    y en el archivo de reporte.
    Recibe el archivo al que le obtendra el md5 y sha2
    '''
    md=md5()
    sha2=sha256()
    with open('resultados.txt','a') as f_resultados:
        with open(archivo, 'r') as f_archivo:
            datos=f_archivo.read()
            md.update(datos)
            sha2.update(datos)
            str_md5 = 'MD5: %s'% md.digest()+'\n'
            str_sha2= 'SHA256: %s'% sha2.digest()+'\n'
            print str_md5, str_sha2
            f_resultados.write(str_md5)
            f_resultados.write(str_sha2)

if __name__ == '__main__':
    with open('resultados.txt','w') as f_resultados:
        f_resultados.write(str(date.now())+'\n')
    sha2_md5(argv[1])
    lee_xml(argv[1])
    obtiene_cantidad()
    imprime_csv()
